Source: powerman
Section: admin
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 bison,
 debhelper-compat (= 13),
 dh-exec,
 flex,
 libcurl4-openssl-dev,
 libgenders0-dev,
 libncurses-dev,
 libjansson-dev,
 libsnmp-dev,
 libwrap0-dev,
 pkgconf,
 systemd-dev [linux-any],
Standards-Version: 4.7.0
Homepage: https://github.com/chaos/powerman
Vcs-Git: https://salsa.debian.org/debian/powerman.git
Vcs-Browser: https://salsa.debian.org/debian/powerman

Package: libpowerman0
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Multi-Arch: same
Description: Client library for Powerman - Centralized PDU management
 PowerMan is a tool for manipulating Power Distribution Units (PDUs) from a
 central location. It is suitable for remote operation in data centers or
 compute cluster environment.
 .
 This package contains the shared client library.

Package: libpowerman0-dev
Section: libdevel
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 libpowerman0 (= ${binary:Version}),
 ${misc:Depends},
Description: Development files for Powerman - Centralized PDU management
 PowerMan is a tool for manipulating Power Distribution Units (PDUs) from a
 central location. It is suitable for remote operation in data centers or
 compute cluster environment.
 .
 This package contains the development files.

Package: powerman
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 adduser,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Centralized Power Distribution Unit (PDU) management
 PowerMan is a tool for manipulating Power Distribution Units (PDUs) from a
 central location. It is suitable for remote operation in data centers or
 compute cluster environment.
 .
 Several RPC varieties are supported natively by PowerMan and
 Expect-like configurability simplifies the addition of new devices.
 .
 This package contains the core system, and includes support for Genders, HTTP
 devices and NCurses user interface.
